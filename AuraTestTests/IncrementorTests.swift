//
//  AuraTestTests.swift
//  AuraTestTests
//
//  Created by Максуд on 30.11.2017.
//  Copyright © 2017 Максуд. All rights reserved.
//

import XCTest
@testable import AuraTest

///Тесты класса IncrementorStore
class IncrementorTests: XCTestCase {
    
    var incrementor: IncrementorStore! = IncrementorStore.shared
    
    override func setUp() {
        super.setUp()
        // Подготовка стора при каждом тесте
        IncrementorStore.prepareStore()
    }
    
    override func tearDown() {
        // Возврат к начальным значеням после каждого теста
        let _ = incrementor.setValues(step: 1, max: Int.max)
        incrementor.flushValue()
        super.tearDown()
    }
    
    ///Тест проверки результата при усновке максимлаьного значение меньше текущего
    /// - Ожидаемый результат: текущее значение обнулится
    func testSetMaxValueLowerCurrentValue(){
        (1...100).forEach { _ in
            let _ = incrementor.incremented()
        }
        let result = incrementor.setValues(step: 1, max: 10)
        XCTAssert(result == true && incrementor.value == 0)
    }
    
    ///Тест проверки результата при усновке максимлаьного значение меньше нуля
    /// - Ожидаемый результат: возрат ошибки
    func testSettingMaxLowerZero(){
        let result = incrementor.setValues(step: 1, max: -1)
        XCTAssert(result == false)
    }
    
    ///Тест проверки результата при усновке максимлаьного значение меньше шага инкрементора
    /// - Ожидаемый результат: возрат ошибки
    func testMaxValueLowerStep(){
        let result = incrementor.setValues(step: 10, max: 1)
        XCTAssert(result == false)
    }
    
    ///Тест проверки результата при превышении максимального значения
    /// - Ожидаемый результат: обнуление текущего значения
    func testReachMaxValue(){
        let _ = incrementor.setValues(step: 1, max: 10)
        (1...11).forEach { _ in
            let _ = incrementor.incremented()
        }
        XCTAssert(incrementor.value == 0)
    }
    
    ///Тест проверки результата при установке шага больше чем максимальное значение
    /// - Ожидаемый результат: возрат ошибки
    func testStepGreaterMaxValue(){
        let result = incrementor.setValues(step: 100, max: 10)
        XCTAssert(result == false)
    }
    
    ///Тест проверки результата при установке шага меньше нуля
    /// - Ожидаемый результат: возрат ошибки
    func testStepLowerZero(){
        let result = incrementor.setValues(step: -1, max: Int.max)
        XCTAssert(result == false)
    }
    
    ///Тест проверки результата при инкрементировании значения
    /// - Ожидаемый результат: значение = число инкрементов * шаг
    func testStepping(){
        incrementor.flushValue()
        let settingResult = incrementor.setValues(step: 10, max: Int.max)
        XCTAssert(settingResult == true)
        
        (1...10).forEach { _ in
            let _ = incrementor.incremented()
        }
        XCTAssert(incrementor.value == 100, "Actual value = \(incrementor.value)")
    }
    
    ///Тест проверки результата при сбросе значения счетчика
    /// - Ожидаемый результат: обнуление текущего значения
    func testFlushing(){
        (1...10).forEach { _ in
            let _ = incrementor.incremented()
        }
        incrementor.flushValue()
        XCTAssert(incrementor.value == 0)
    }
    
    /// Тест проверки сохранения значения инкрементора
    /// - Ожидаемый результат: сохраненное значение икнеметора
    func testValueSavingData(){
        incrementor.flushValue()
        let _ = incrementor.incremented()
        let saved = UserDefaults.standard.integer(forKey: StoreSettingsKeys.value.key)
        XCTAssert(saved == 1)
    }
    
    /// Тест проверки сохранения максимльного значения инкрементора
    /// - Ожидаемый результат: сохраненное максимального значение инкрементора
    func testMaxValueSavingData(){
        let _ = incrementor.setValues(step: 1, max: 100)
        let saved = UserDefaults.standard.integer(forKey: StoreSettingsKeys.maxValue.key)
        XCTAssert(saved == 100)
    }
    
    /// Тест проверки сохранения шага инкрементора
    /// - Ожидаемый результат: сохраненное шага инкрементора
    func testStepValueSavingData(){
        let _ = incrementor.setValues(step: 100, max: Int.max)
        let saved = UserDefaults.standard.integer(forKey: StoreSettingsKeys.step.key)
        XCTAssert(saved == 100)
    }
    
    /// Тест проверки начальных значений IncrementoreStore
    /// - значение value: 0
    /// - шаг step: 1
    /// - максимльное значение maxValue: Int.max
    func testPrepareStoreFirstTime(){
        IncrementorStore.prepareStore()
        XCTAssert(incrementor.value == 0)
        XCTAssert(incrementor.step == 1)
        XCTAssert(incrementor.maxValue == Int.max)
    }

    
    /// Тест проверки инициализации IncrementorStore сохраненными ранее значениями
    func testPrepareStoreNotInitial(){
        UserDefaults.standard.set(true, forKey: StoreSettingsKeys.start.key)

        let result = incrementor.setValues(step: 10, max: 100)
        XCTAssert(result == true)
        
        (1...3).forEach { _ in
            _ = incrementor.incremented()
        }
        
        IncrementorStore.prepareStore()
        XCTAssert(incrementor.value == 30)
        XCTAssert(incrementor.step == 10)
        XCTAssert(incrementor.maxValue == 100)
    }
}
