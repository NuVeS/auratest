//
//  AuraTestUITests.swift
//  AuraTestUITests
//
//  Created by Максуд on 30.11.2017.
//  Copyright © 2017 Максуд. All rights reserved.
//

import XCTest
@testable import AuraTest

/// UI тест экрана Меню
/// Добавления аргумента при запуске. Для инициализации IncrementorStore начальными значения при UI тесте
class MenuUITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        let app = XCUIApplication()
        app.launchArguments += ["UI-Testing"]
        app.launch()
    }
    
    override func tearDown() {
        XCUIApplication().launchArguments.removeAll()
        super.tearDown()
    }
    
    /// Тест проверки начального значения при запуске
    /// - Ожидаемый результат: 0 - начальное значение инкрементора
    func testStartingValue() {
        XCTAssert(XCUIApplication().buttons["0"].exists)
    }
    
    /// Тест проверки изменения отображаемого значения при инкременте
    func testValueChange(){
        (1...4).forEach{ i in
            XCUIApplication().buttons["\(i-1)"].tap()
        }
        XCTAssert(XCUIApplication().buttons["4"].exists)
    }
        
}
