//
//  test.swift
//  AuraTestUITests
//
//  Created by Максуд on 30.11.2017.
//  Copyright © 2017 Максуд. All rights reserved.
//

import XCTest

class SettingsUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        let app = XCUIApplication()
        app.launchArguments += ["UI-Testing"]
        app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    ///Проверка данных на начальные значения в IncrementorStore
    func testSettingStartValue(){
        let app = XCUIApplication()
        app.navigationBars["Меню"].buttons["Настройки"].tap()
        XCTAssert(app.textFields["1"].exists)
        XCTAssert(app.textFields["\(Int.max)"].exists)
        
        let button = app.navigationBars["Настройки"].buttons["Сохранить"]
        XCTAssert(button.isEnabled == false)
    }
    
    /// Тест недоступности кнопки сохранить при пустых полях
    func testEmptyFieldsSaveDisabled(){
        let app = XCUIApplication()
        app.navigationBars["Меню"].buttons["Настройки"].tap()
        let element = app.otherElements.containing(.navigationBar, identifier:"Настройки").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let stepTF = element.children(matching: .textField).element(boundBy: 0)
        stepTF.tap()
        
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssert(app.navigationBars["Настройки"].buttons["Сохранить"].isEnabled == false)
        
        stepTF.typeText("20")
        let maxValueTF = element.children(matching: .textField).element(boundBy: 1)
        maxValueTF.tap()
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.press(forDuration: 4.5)
        XCTAssert(app.navigationBars["Настройки"].buttons["Сохранить"].isEnabled == false)
    }
    
    /// Тест на недоступность сохранения при неверных значениях шага
    func testErrorValuesStepSaveDisabled(){
        let app = XCUIApplication()
        app.navigationBars["Меню"].buttons["Настройки"].tap()
        let element = app.otherElements.containing(.navigationBar, identifier:"Настройки").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let stepTF = element.children(matching: .textField).element(boundBy: 0)
        let maxValueTF = element.children(matching: .textField).element(boundBy: 1)
        
        // Отрицательное значение
        stepTF.tap()
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        stepTF.typeText("-1")
        XCTAssert(app.navigationBars["Настройки"].buttons["Сохранить"].isEnabled == false)
        
        // Значение больше максимального
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.press(forDuration: 1.5)
        stepTF.typeText("1")
        maxValueTF.tap()
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.press(forDuration: 4.5)
        maxValueTF.typeText("10")
        stepTF.tap()
        stepTF.typeText("20")
        XCTAssert(app.navigationBars["Настройки"].buttons["Сохранить"].isEnabled == false)
    }
    
    /// Тест на недоступность сохранения при неверных значениях максимума
    func testErrorMaxValueSaveDisabled(){
        let app = XCUIApplication()
        app.navigationBars["Меню"].buttons["Настройки"].tap()
        let element = app.otherElements.containing(.navigationBar, identifier:"Настройки").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let stepTF = element.children(matching: .textField).element(boundBy: 0)
        let maxValueTF = element.children(matching: .textField).element(boundBy: 1)
        
        //Отрицательное значение
        maxValueTF.tap()
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.press(forDuration: 4.5)
        maxValueTF.typeText("-1")
        XCTAssert(app.navigationBars["Настройки"].buttons["Сохранить"].isEnabled == false)
        
        // Меньше шага
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.press(forDuration: 4.5)
        maxValueTF.typeText("100")
        stepTF.tap()
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.press(forDuration: 1.5)
        stepTF.typeText("20")
        maxValueTF.tap()
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.press(forDuration: 1.5)
        maxValueTF.typeText("10")
        XCTAssert(app.navigationBars["Настройки"].buttons["Сохранить"].isEnabled == false)
    }
    
    /// Проверка сброса счетчика
    func testFlush(){
        let app = XCUIApplication()
        (1...4).forEach{ i in
            XCUIApplication().buttons["\(i-1)"].tap()
        }
        app.navigationBars["Меню"].buttons["Настройки"].tap()
        let element = app.otherElements.containing(.navigationBar, identifier:"Настройки").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        element.buttons["Сброс счетчика"].tap()
        app.navigationBars["Настройки"].buttons["Меню"].tap()
        XCTAssert(app.buttons["0"].exists)
    }
    
    /// Проверка сохранения данных
    func testSaving(){
        let app = XCUIApplication()
        (1...10).forEach{ i in
            app.buttons["\(i-1)"].tap()
        }
        app.navigationBars["Меню"].buttons["Настройки"].tap()
        
        let element = app.otherElements.containing(.navigationBar, identifier:"Настройки").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let maxValueTF = element.children(matching: .textField).element(boundBy: 1)
        maxValueTF.tap()
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.press(forDuration: 4.5)
        maxValueTF.typeText("5")
        
        app.navigationBars["Настройки"].buttons["Сохранить"].tap()
        XCTAssert(app.buttons["0"].exists)
    }
    
    ///Тест появления и скрытия клавиатры
    func testHidingKeyboard(){
        let app = XCUIApplication()
        app.navigationBars["Меню"].buttons["Настройки"].tap()
        let element = app.otherElements.containing(.navigationBar, identifier:"Настройки").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let maxValueTF = element.children(matching: .textField).element(boundBy: 1)
        let stepTF = element.children(matching: .textField).element(boundBy: 0)
        
        // Клавиатура появляется на шаге и пропадает при тапе на вью
        stepTF.tap()
        XCTAssert(app.keyboards.count > 0)
        element.tap()
        XCTAssert(app.keyboards.count == 0)
        
        // Клавиатура появляется при выборе максимума и пропадает при тапе на вью
        maxValueTF.tap()
        XCTAssert(app.keyboards.count > 0)
        element.tap()
        XCTAssert(app.keyboards.count == 0)
    }
}
