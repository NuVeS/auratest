//
//  GeneralUITests.swift
//  AuraTestTests
//
//  Created by Максуд on 02.12.2017.
//  Copyright © 2017 Максуд. All rights reserved.
//

import XCTest

class GeneralUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        let app = XCUIApplication()
        app.launchArguments += ["UI-Testing"]
        app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /// Тест действия установки шага на счетчик
    func testSettingStepEffect(){
        let app = XCUIApplication()
        (1...3).forEach{ i in
            app.buttons["\(i-1)"].tap()
        }
        app.navigationBars["Меню"].buttons["Настройки"].tap()
        
        let element = app.otherElements.containing(.navigationBar, identifier:"Настройки").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let stepTF = element.children(matching: .textField).element(boundBy: 0)
        stepTF.tap()
        app/*@START_MENU_TOKEN@*/.keys["Delete"]/*[[".keyboards.keys[\"Delete\"]",".keys[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.press(forDuration: 4.5)
        stepTF.typeText("5")
        
        app.navigationBars["Настройки"].buttons["Сохранить"].tap()
        app.buttons["3"].tap()
        
        XCTAssert(app.buttons["8"].exists)
    }
    
}
