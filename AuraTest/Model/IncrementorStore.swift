//
//  IncrementorStore.swift
//  AuraTest
//
//  Created by Максуд on 30.11.2017.
//  Copyright © 2017 Максуд. All rights reserved.
//

import Foundation

/// Возможные ошибки валидации максимального значения
/// lowerZero максимальное значение меньше нуля
/// lowerStep максимальное значение меньше шага инкремента
/// wrongValue Не числовое значение
enum SettingsMaxValueError : String{
    case lowerZero = "Максимальное значение должно быть больше 0"
    case lowerStep = "Максимальное значение должно быть больше шага"
    case wrongValue = "Не числовое значение"
}

/// Возможные ошибки валидации шага инкрементора
/// lowerZero шаг инкрементора меньше нуля
/// greaterMax шаг инкреметора больше максимльного значения
/// wrongValue Не числовое значение
enum SettingsStepError : String{
    case lowerZero = "Шаг должен быть больше 0"
    case greaterMax = "Шаг должен быть меньше максимального значения"
    case wrongValue = "Не числовое значение"
}

/// Возможные ошибки, возникающие в Store.
/// validatingMaxValue представляет ошибку валидации максимального значения
/// validatingStepValue представляет ошибку валидации шага инкрементора
enum StoreErrors : Error{
    case validatingMaxValue
    case validatingStepValue
}

/// IncrementorStore использует UserDefaults для хранения текущих значений между перезапусками приложения
/// StoreSettingsKeys перечисляет ключи для получения настроек
enum StoreSettingsKeys : String{
    case value = "CurrentValue"
    case maxValue = "MaximumValue"
    case step = "Step"
    case start = "hasStarted"
    
    var key: String{
        get{
            return self.rawValue
        }
    }
}

/// Класс IncrementorStore представляет хранилище для доступа к тещему значению, шага инкрементора и максимального значения
/// IncrementorStore является синглтоном, так что для работы с ним необходимо получить shared значение: IncrementorStore.shared
/// Перед использованием необоходимо проинициализировать значения хранящимися в UserDefaults или начальными значениями при первом запуске методом класс prepareStore
/// Лучшим местом для инициализации является AppDelegate или другой объект производящий преднастройку приложения:
///         IncrementorStore.prepareStore()
/// Начальные значения:
///         value: 0
///         step: 1
///         step: Int.max
class IncrementorStore{
    
    //MARK: - Private
    
    /// Приватная переменная _value хранит текущее значение инкрементора
    /// При присвоении значения записывает данные в UserDefaults
    private var _value: Int!{
        didSet{
            saveValue(value: _value, forKey: StoreSettingsKeys.value.key)
        }
    }
    
    /// Приватная переменная _maxValue хранит максимальное значение инкрементора
    /// При присвоении значения записывает данные в UserDefaults
    private var _maxValue: Int!{
        didSet{
            if _value > _maxValue{
                _value = 0
            }
            saveValue(value: _maxValue, forKey: StoreSettingsKeys.maxValue.key)
        }
    }
    
    /// Приватная переменная _step хранит шаг инкрементора
    /// При присвоении значения записывает данные в UserDefaults
    private var _step: Int!{
        didSet{
            saveValue(value: _step, forKey: StoreSettingsKeys.step.key)
        }
    }
    
    //MARK: - Public
    
    /// Публичная переменная value - getOnly. Возвращает _value - текущее значение инкрементора
    var value: Int{
        get{
            return _value
        }
    }
    
    /// Публичная переменная maxValue - getOnly. Возвращает _maxValue - максимальное значение инкрементора
    var maxValue: Int{
        get{
            return _maxValue
        }
    }
    
    /// Публичная переменная step - getOnly. Возвращает _step - шаг инкрементора
    var step: Int {
        get{
            return _step
        }
    }
    
    /// Синглтон объект класса IncrementorStore. Работа с классом производится только через него
    static var shared: IncrementorStore = IncrementorStore()
    
    /// Метод для инициализации объекта-синглтона shared
    class func prepareStore(){
        let userDefaults = UserDefaults.standard
        if userDefaults.bool(forKey: StoreSettingsKeys.start.key){
            shared._value = userDefaults.integer(forKey: StoreSettingsKeys.value.key)
            shared._maxValue = userDefaults.integer(forKey: StoreSettingsKeys.maxValue.key)
            shared._step = userDefaults.integer(forKey: StoreSettingsKeys.step.key)
        }else{
            shared._value = 0
            shared._maxValue = Int.max
            shared._step = 1
            userDefaults.set(true, forKey: StoreSettingsKeys.start.key)
        }
    }
    
    //MARK: - Private interface
    /// Сохранение значения в UserDefaults
    ///
    /// - Parameter value: сохраняемое значение
    /// - Parameter key: ключ для сохранения
    private func saveValue(value: Int, forKey key: String){
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    //MARK: - Public interface
    
    /// Метод для инкремента текущего значени с текущим шагом
    /// - returns: Инкрементированное значение
    func incremented() -> Int{
        if _value + step <= maxValue{
            _value! += _step!
        }else{
            _value = 0
        }
        return _value
    }
    
    /// Метод для установки значений шага и максимального значения
    /// - Parameter step: новое значение шага
    /// - Parameter max: новое значение максимума
    /// - returns: Ошибку false если новые значение не удовлетворяет условиям. true если значения установлены
    func setValues(step: Int, max: Int) -> Bool{
        if let _ = validateStepValue(value: step, withMaxValue: max){
            return false
        }
        if let _ = validateMaxValue(value: max, withStepValue: step){
            return false
        }
        _step = step
        _maxValue = max
        return true
    }
    
    /// Метод для сброса значения инкрементора
    func flushValue(){
        _value = 0
    }
    
    /// Метод валидации максимального значения
    /// - Parameter value: Проверяемое значение максимума
    /// - Parameter step: Значение шага с которым проверяется максимум
    /// - returns: Ошибку SettingsMaxValueError если новое значение не удовлетворяет условиям. nil если удовлетворяем
    func validateMaxValue(value: Int, withStepValue step: Int) -> SettingsMaxValueError?{
        if value < 0{
            return SettingsMaxValueError.lowerZero
        }
        if value < step{
            return SettingsMaxValueError.lowerStep
        }
        return nil
    }
    
    /// Метод валидации шага инкрементора
    /// - Parameter value: Проверяемое значение шага
    /// - Parameter max: Значение максимум с которым шаг
    /// - returns: Ошибку SettingsStepError если новое значение не удовлетворяет условиям. nil если удовлетворяем
    func validateStepValue(value: Int, withMaxValue max: Int) -> SettingsStepError?{
        if value < 0{
            return SettingsStepError.lowerZero
        }
        if value > max{
            return SettingsStepError.greaterMax
        }
        return nil
    }
}
