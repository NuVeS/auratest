//
//  ViewController.swift
//  AuraTest
//
//  Created by Максуд on 30.11.2017.
//  Copyright © 2017 Максуд. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    var store: IncrementorStore = IncrementorStore.shared
    @IBOutlet weak var incrementButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setButtonTitle(value: store.value)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let id = segue.identifier else { return }
        if id == "DetailSegue"{
            let vc = segue.destination as? SettingsViewController
            vc?.store = store // При переходе на Экран настроек инъектируем ему Store
        }
    }
    
    /// Action кнопки инкрементора. Инкрементирует текущее значение и обновляет UI
    @IBAction func increment(_ sender: Any) {
        setButtonTitle(value: store.incremented())
    }
    
    /// Устанавливает title для кнопки инкрементора
    private func setButtonTitle(value: Int){
        incrementButton.setTitle("\(value)", for: .normal)
    }
}

