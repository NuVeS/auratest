//
//  SettingsViewController.swift
//  AuraTest
//
//  Created by Максуд on 30.11.2017.
//  Copyright © 2017 Максуд. All rights reserved.
//

import UIKit
import DTTextField

class SettingsViewController: UIViewController {
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var incrementorTF: DTTextField!
    @IBOutlet weak var maxValueTF: DTTextField!
    
    var store: IncrementorStore?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupValues()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    private func setupValues(){
        guard let maxValue = store?.maxValue else { return }
        guard let step = store?.step else { return }
        
        maxValueTF.text = "\(maxValue)"
        incrementorTF.text = "\(step)"
        saveButton.isEnabled = false
    }
    
    /// Action кнопки сброса текущего значение инкрементора
    @IBAction func flushCount(_ sender: Any) {
        store?.flushValue()
    }
    
    /// Action кнопки сохранения введенных шага инкрементора и максимальго значений
    /// Перед сохранением валидирует данные
    @IBAction func save(_ sender: Any) {
        guard let max = Int(maxValueTF.text!) else { return }
        guard let step = Int(incrementorTF.text!) else { return }
        
        if store?.setValues(step: step, max: max) == true{
            hapticEvent(event: .success)
            navigationController?.popViewController(animated: true)
        }else{
            hapticEvent(event: .error)
        }
    }
}

// MARK: - UITextFieldDelegate
extension SettingsViewController : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let nsString = textField.text as NSString?
        textField.text = nsString?.replacingCharacters(in: range, with: string)
        
        let stepValid = checkStepValue()
        let maxValid = checkMaxValue()
        
        // Если значение валидные, сделать доступной кнопку сохранения
        if stepValid && maxValid{
            saveButton.isEnabled = true
        }else{
            saveButton.isEnabled = false
        }
        
        return false
    }
    
    /// Метод для валидации максимального значения
    /// - returns: True если значение валидно.
    ///            False если значение не валидно.
    private func checkMaxValue() -> Bool{
        guard let value = Int(maxValueTF.text!) else {
            maxValueTF.showError(message: SettingsMaxValueError.wrongValue.rawValue)
            return false
        }
        guard let step = Int(incrementorTF.text!) else {
            return false
        }
        if let error = store?.validateMaxValue(value: value, withStepValue: step){
            maxValueTF.showError(message: error.rawValue)
            return false
        }
        return true
    }
    
    /// Метод для валидации шага инкрементора
    /// - returns: True если значение валидно.
    ///            False если значение не валидно.
    private func checkStepValue() -> Bool{
        guard let value = Int(incrementorTF.text!) else {
            incrementorTF.showError(message: SettingsStepError.wrongValue.rawValue)
            return false
        }
        guard let maxValue = Int(maxValueTF.text!) else {
            return false
        }
        if let error = store?.validateStepValue(value: value, withMaxValue: maxValue){
            incrementorTF.showError(message: error.rawValue)
            return false
        }
        return true
    }

    private func hapticEvent(event: UINotificationFeedbackType){
        UINotificationFeedbackGenerator().notificationOccurred(event)
    }
}
